lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require '{{cookiecutter.project_slug}}/version'

Gem::Specification.new do |spec|
  spec.name          = '{{cookiecutter.project_slug}}'
  spec.version       = {{cookiecutter.project_klass}}::VERSION
  spec.authors       = ['{{cookiecutter.author_name}}']
  spec.email         = ['{{cookiecutter.author_email}}']
  spec.summary       = '{{cookiecutter.project_summary}}'
  spec.homepage      = '{{cookiecutter.project_home}}'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']
end
